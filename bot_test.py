import discord
import pytest
import sys
from bot import Bot


def test_avoid_self_messages():
    bot = Bot()
    bot.on_message(MockUser(1), MockMessage(2))


class MockUser:
    def __init__(self, id):
        self.id = id


class MockMessage:
    def __init__(self, id):
        self.id = id


if __name__ == "__main__":
    sys.exit(pytest.main([__file__]))