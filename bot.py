import json
import logging
import pathlib
import time
from statistics import median, mean
from typing import Dict, List

import discord

from game import Game, Guess, HistoricalGame, AlreadyGuessedException, AlreadyDoneException, NoGuessesException

ALL_COMMANDS = {'help', '?', 'start', 'complete', 'show', 'freeze'}

logger = logging.getLogger('bot')


class Bot:
    def __init__(self, games=None, history_path=None):
        if games is None:
            games = {}
        self.games: Dict[Game] = games
        self.history_path: pathlib.Path = history_path

    @staticmethod
    async def print_help(channel: discord.TextChannel):
        await channel.send('Channel moderators can tell me to `start <game name>` or `complete <correct answer>`.')

    @staticmethod
    async def permissions_check(user: discord.Member, channel: discord.TextChannel) -> bool:
        if channel.permissions_for(user).manage_channels:
            return True
        await channel.send('You need to have manage_channel permissions to send me commands.')
        return False

    async def start_cmd(self, channel: discord.TextChannel, game: Game, name: str):
        if game:
            await channel.send(
                f'There is already a game in progress in this channel: {game.name}. End it first!')
        else:
            game = Game(channel.id, name)
            self.games[channel.id] = game
            await channel.send(f'Guessing game `{game.name}` started. To participate, simply send a message in '
                               'this channel/thread with your guess. To update your guess, send a new message.')

    async def complete_cmd(self, channel: discord.TextChannel, game: Game, correct_number: int):
        if not game:
            await channel.send('No guessing game has been started yet. Start one by sending me an @ that says '
                               '`start <game name>`')
        else:
            try:
                winners = game.complete_game(correct_number)
                response = f'`{game.name}` is complete! '
                distance = abs(winners[0].guessed_number - correct_number)
                if len(winners) > 1:
                    distance = abs(winners[0].guessed_number - correct_number)
                    response += f'Two way tie between {winners[0]} and {winners[1]}. Both were {distance} away.'
                else:
                    response += f'{winners[0]} wins with a distance of {distance}.'
                await channel.send(response)
                for winner in winners:
                    winning_msg = await channel.fetch_message(winner.message_id)
                    await winning_msg.add_reaction('🏆')
            except NoGuessesException:
                await channel.send('No one guessed!')
            if self.history_path:
                self.history_path.mkdir(parents=True, exist_ok=True)
                record_path = self.history_path / time.strftime(f'%Y%m%d-%H%M%S_{game.channel_id}-{game.name}')
                history = HistoricalGame(game).__dict__
                with record_path.open('w') as f:
                    logger.info(f'saving completed game: {history}')
                    json.dump(history, f)
            del self.games[channel.id]

    @staticmethod
    async def list_cmd(channel: discord.TextChannel, game: Game):
        if not game:
            await channel.send('No guessing game has been started yet. Start one by sending me an @ that says '
                               '`start <game name>`')
        else:
            if len(game.guesses) == 0:
                await channel.send('No guesses have been made yet.')
                return
            sorted_guesses = sorted(game.guesses.keys())
            await channel.send(f'Current guesses: `{sorted_guesses}`\nMean: `{mean(sorted_guesses)}`\n'
                               f'Median: `{median(sorted_guesses)}`')

    @staticmethod
    async def freeze_cmd(channel: discord.TextChannel, game: Game):
        if not game:
            await channel.send('No guessing game has been started yet. Start one by sending me an @ that says '
                               '`start <game name>`')
        else:
            game.done = True
            await channel.send(f'`{game.name}` has been frozen, no more guesses will be accepted. The game can be '
                               'finalized with `complete <correct answer>`')

    async def handle_mention(self, msg: discord.Message, game: Game):
        tokens: List[str] = msg.content.strip().split(' ')
        command = tokens[1]

        if command not in ALL_COMMANDS:
            return

        logger.debug(f'got command: {tokens} in {msg.channel.name}')

        if command == 'help' or command == '?':
            await self.print_help(msg.channel)
        elif command == 'start':
            if not await self.permissions_check(msg.author, msg.channel):
                return
            if len(tokens) < 3:
                await msg.channel.send('Please provide the name of the game to start with `start <game name>`.')
                return
            await self.start_cmd(msg.channel, game, tokens[2])
        elif command == 'show':
            await self.list_cmd(msg.channel, game)
        elif command == 'freeze':
            if not await self.permissions_check(msg.author, msg.channel):
                return
            await self.freeze_cmd(msg.channel, game)
        elif command == 'complete':
            if not await self.permissions_check(msg.author, msg.channel):
                return
            if len(tokens) < 3:
                await msg.channel.send('Please provide the correct answer with `complete <correct number>`.')
                return
            correct_number = tokens[2]
            if not correct_number.isnumeric():
                await msg.channel.send('Please provide a positive integer for the correct number.')
                return
            await self.complete_cmd(msg.channel, game, int(tokens[2]))

    async def on_message(self, user: discord.User, msg: discord.Message):
        if msg.author == user:
            return

        logging.debug(f'event: message from {msg.author.name} in {msg.channel.name}')

        # look for a started game in the current channel/thread
        game = self.games.get(msg.channel.id)

        # TODO role mention?
        if user in msg.mentions:
            await self.handle_mention(msg, game)

        if msg.channel.id not in self.games:
            return

        content = msg.content.strip()

        if content.isnumeric():
            guess = int(content)  # TODO float?
            try:
                game.add_guess(Guess(msg.author.id, msg.id, guess))
                if guess == 69 or guess == 420:
                    await msg.add_reaction('😏')
                await msg.add_reaction('👍')
            except AlreadyGuessedException as e:
                await msg.add_reaction('❌')
                await msg.reply(str(e), mention_author=True)
            except AlreadyDoneException:
                logging.error(f'{msg.author.name} Tried to add a guess for a game that was already completed in '
                              f'{msg.guild}/{msg.channel}')

    @staticmethod
    async def on_ready(user: discord.User, guilds: List[discord.Guild]):
        logging.info(f'{user} has connected to Discord!')
        for guild in guilds:
            logging.info(f'\t- {guild.name} (id: {guild.id})')
