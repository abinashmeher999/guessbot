import pytest
import sys
from game import Game, Guess


def test_game_add_guess():
    game = Game(0, 'test')
    assert len(game.guesses) == 0
    game.add_guess(Guess(0, 0, 10))
    assert len(game.guesses) == 1


def test_game_add_guess_complete():
    game = Game(0, 'test')
    game.add_guess(Guess(0, 0, 10))
    game.complete_game(10)
    with pytest.raises(Exception):
        game.add_guess(Guess(0, 0, 10))


def test_game_add_guess_duplicate():
    game = Game(0, 'test')
    game.add_guess(Guess(0, 0, 10))
    with pytest.raises(Exception):
        game.add_guess(Guess(1, 1, 10))
    assert len(game.guesses) == 1


def test_game_add_guess_overwrite():
    game = Game(0, 'test')
    game.add_guess(Guess(0, 0, 10))
    game.add_guess(Guess(0, 1, 11))
    assert len(game.guesses) == 1
    assert game.guesses[11].author_id == 0


def test_game_complete():
    game = Game(0, 'test')
    game.add_guess(Guess(2, 3, 10))
    winners = game.complete_game(10)
    assert len(winners) == 1
    assert winners[0].guessed_number == 10
    assert winners[0].author_id == 2
    assert winners[0].message_id == 3


def test_game_complete_no_guesses():
    game = Game(0, 'test')
    with pytest.raises(Exception):
        game.complete_game(10)


def test_game_complete_tie():
    game = Game(0, 'test')
    game.add_guess(Guess(1, 2, 10))
    game.add_guess(Guess(2, 3, 12))
    winners = game.complete_game(11)
    assert len(winners) == 2


def test_game_complete_inexact():
    game = Game(0, 'test')
    game.add_guess(Guess(1, 2, 10))
    game.add_guess(Guess(2, 3, 12))
    winners = game.complete_game(13)
    assert len(winners) == 1
    assert winners[0].guessed_number == 12
    assert winners[0].author_id == 2
    assert winners[0].message_id == 3


if __name__ == "__main__":
    sys.exit(pytest.main([__file__]))
