import os
import logging
import pickle
from pathlib import Path
from typing import Dict

import discord
from bot import Bot
from dotenv import load_dotenv

from game import Game

BASE_PERSIST_PATH = Path('/persist')


def run():
    logging.basicConfig(level=logging.INFO)
    load_dotenv()

    games = {}
    data_path = BASE_PERSIST_PATH / 'data'
    if data_path.exists():
        logging.info('found persisted data')

        with data_path.open('rb') as f:
            games: Dict[Game] = pickle.load(f)
            logging.info(f'found {len(games)} in progress game(s):')
            for channel_id, game in games.items():
                logging.info(f'\t-{game.name} in {channel_id}')

    client = discord.Client(intents=discord.Intents(messages=True, guilds=True))

    bot = Bot(games, BASE_PERSIST_PATH / 'history')

    @client.event
    async def on_message(msg: discord.Message):
        await bot.on_message(client.user, msg)

    @client.event
    async def on_ready():
        if data_path.exists():
            logging.info('clearing existing data')
            data_path.unlink()
        await bot.on_ready(client.user, client.guilds)

    @client.event
    async def on_raw_message_delete(payload: discord.RawMessageDeleteEvent):
        # TODO clear a guess that has already been made
        pass

    @client.event
    async def on_raw_bulk_message_delete(payload: discord.RawBulkMessageDeleteEvent):
        # TODO clear a guess that has already been made
        pass

    @client.event
    async def on_guild_channel_delete(channel: discord.abc.GuildChannel):
        # TODO clear any in progress games for that channel
        pass

    @client.event
    async def on_guild_remove(channel: discord.abc.GuildChannel):
        # TODO clear all existing games for that guild
        pass

    try:
        client.run(os.getenv('DISCORD_TOKEN'))
    finally:
        logging.warning('finally')
        if len(bot.games) > 0:
            logging.info('found some in progress games to save...')
            if data_path.parent.exists() and data_path.parent.is_dir():
                with data_path.open('wb') as f:
                    pickle.dump(bot.games, f, pickle.HIGHEST_PROTOCOL)
                logging.info('save complete.')
            else:
                logging.warning('unable to write to persistence directory')


if __name__ == '__main__':
    run()
